# 인공 신경망 소개 <sup>[1](#footnote_1)</sup>

> <font size="3">인공 신경망의 기본과 작동 방식에 대해 알아본다.</font>

## 목차

1. [인공 신경망이란?](./intro-to-ann.md#sec_01)
1. [인공 신경망의 학습 방법?](./intro-to-ann.md#sec_02)
1. [인공 신경망의 종류](./intro-to-ann.md#sec_03)
1. [인공 신경망의 어플리케이션](./intro-to-ann.md#sec_04)
1. [인공 신경망의 과제와 한계](./intro-to-ann.md#sec_015)
1. [마치며](./intro-to-ann.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 8 — Introduction to Artificial Neural Networks](https://medium.datadriveninvestor.com/ml-tutorial-8-introduction-to-artificial-neural-networks-c81b00c2b1ee?sk=589a0dcc557fdafc7c5ea3654e9d3d4f)를 편역한 것입니다.
